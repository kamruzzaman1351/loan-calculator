// UI Variables
const loadFormUI = document.getElementById("loan-form"),
      amountUI = document.getElementById("amount"),
      interestUI = document.getElementById("interest"),
      yearsUI = document.getElementById("years"),
      resultsDivUI = document.getElementById("results"),
      monthlyPaymentUI = document.getElementById("monthly-payment"),
      totalPaymentUI = document.getElementById("total-payment"),
      totalInterestUI = document.getElementById("total-interest"),
      loadingUI = document.getElementById("loading");

// Event Listener
loadFormUI.addEventListener("submit", loanCalculate);
// Loan Calculate Event Function
function loanCalculate(e) {
    e.preventDefault();
    resultsDivUI.style.display = "none";
    loadingUI.style.display = "block";
    setTimeout(calculateLoan, 2500);
}


// Loan Calculation
function calculateLoan() {        
    const princilapAmount = parseFloat(amountUI.value);
    const interestRate = parseInt(interestUI.value);
    const totalYear = parseInt(yearsUI.value);
    // Monthly Payment
    monthlyPayment(princilapAmount, interestRate, totalYear);
    loadingUI.style.display = "none";   
}


// Monthly Payment Calculation
function monthlyPayment(P,I,Y) {
    const interestPerMonth = I/100/12;
    const totalMonth = Y * 12;
    const x = Math.pow(1+interestPerMonth, totalMonth);
    const monthlyPay  = (P * interestPerMonth * x ) / (x -1);
    if(isFinite(monthlyPay)) {
        monthlyPaymentUI.value = monthlyPay.toFixed(3);
        totalPaymentUI.value = (monthlyPay * totalMonth).toFixed(3);
        totalInterestUI.value = ((monthlyPay * totalMonth)- P).toFixed(3);
        resultsDivUI.style.display = "block";
    } else {
        showErrorMsg("Something went wrong! Please Check the field again.");
        resultsDivUI.style.display = "none";
    }

}

// Show Error Massage
function showErrorMsg(error) {
    const errorDiv = document.createElement("div");
    errorDiv.className = "alert alert-danger";
    errorDiv.appendChild(document.createTextNode(error));
    // Get Element From UI
    const cardUI = document.querySelector(".card");
    const headingUI = document.querySelector(".heading");
    // Insert ErrorDiv Befor the Heading
    cardUI.insertBefore(errorDiv, headingUI);
    // Clear Error Msg after few second
    setTimeout( clearError, 3000);
}

// Clear Error Msg
function clearError() {
    document.querySelector(".alert").remove();
}